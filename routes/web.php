<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('giris-yap', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('giris-yap', 'Auth\LoginController@login');
Route::post('cikis-yap', 'Auth\LoginController@logout')->name('logout');
Route::get('sifremi-unuttum', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('forgotpassword');
Route::post('sifremi-unuttum', 'Auth\ForgotPasswordController@sendResetPassword');

Route::get('kayit-ol', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('kayit-ol', 'Auth\RegisterController@register');

Route::prefix('hesabim')->namespace('Member')->middleware('auth', 'normal_user')->group(function () {
    // Profil
    Route::get('/', 'UserController@index')->name('profile');
    // Sepet
    Route::post('sepete-ekle', 'BasketController@add')->name('basket.add');
    Route::get('sepet', 'BasketController@index')->name('basket');
    Route::post('sepet-sil', 'BasketController@delete')->name('basket.delete');

    Route::get('alisveris-tamamla', 'CheckoutController@index')->name('checkout');
    Route::post('alisveris-tamamla', 'CheckoutController@complete')->name('checkout.complete');

});

Route::prefix('admin')->namespace('Admin')->middleware('auth', 'super_user')->group(function () {

    Route::get('dashboard', 'DashboardController@index')->name('admin.dashboard');

    Route::get('urun', 'ProductController@index')->name('admin.product');
    Route::get('urun-ekle', 'ProductController@create')->name('admin.product.create');
    Route::post('urun-ekle', 'ProductController@save');
    Route::get('urun-guncelle/{id}', 'ProductController@update')->name('admin.product.update');
    Route::post('urun-guncelle/{id}', 'ProductController@save');
    Route::post('urun-sil/{id}', 'ProductController@delete')->name('admin.product.delete');

    Route::get('siparis', 'OrderController@index')->name('admin.order');
    Route::get('siparis/{id}', 'OrderController@view')->name('admin.order.view');
    Route::post('siparis-onayla/{id}', 'OrderController@approve')->name('admin.order.approve');
    Route::post('siparis-iptal/{id}', 'OrderController@cancel')->name('admin.order.cancel');

});
