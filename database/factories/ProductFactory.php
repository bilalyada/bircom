<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'price' => $faker->randomDigit,
        'description' => $faker->paragraph,
        'currency_id' => function () {
            return \App\Model\Currency::inRandomOrder()->first()->id;
        },
        'image' => null,
    ];
});
