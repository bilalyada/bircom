<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Admini Ekle
        $user['name'] = 'Admin';
        $user['username'] = 'admin';
        $user['password'] = '123456';
        $user['email'] = 'admin@bircom.com';
        $user['type'] = \App\User::ROLE_ADMIN;
        $user['email_verified_at'] = \Carbon\Carbon::now();
        \App\User::create($user);

        // Test kullanıcı Ekle
        $testuser['name'] = 'Test User';
        $testuser['username'] = 'testuser';
        $testuser['password'] = '123456';
        $testuser['email'] = 'testuser@bircom.com';
        $testuser['email_verified_at'] = \Carbon\Carbon::now();
        \App\User::create($testuser);

        // Currency Ekle
        $currency['code'] = 'TRY';
        $currency['readable'] = '₺';
        \App\Model\Currency::create($currency);

        // 20 Tane Ürün Ekle
        $count = 20;
        $genres = factory(\App\Model\Product::class, $count)->create();
    }
}
