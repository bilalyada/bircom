<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Order;
use Illuminate\Support\Facades\Session;

class OrderController extends Controller
{

    public function index()
    {
        $models = Order::with(['user'])
                    ->orderBy('id', 'desc')
                    ->paginate(10);
        return view('admin.order.index', compact('models'));
    }

    public function view($id)
    {
        $model = Order::with('items')->findOrFail($id);
        return view('admin.order.view', compact('model'));
    }
    // Onaylama İşlemi
    public function approve($id) {
        $model = Order::findOrFail($id);
        $model->status = Order::STATUS_APPROVED;
        if($model->save()) {
            Session::flash('success_message', 'Sipariş başarılı bir şekilde onaylandı!');
        }else {
            Session::flash('error_message', 'Beklenmedik bir hata meydana geldi!');
        }

        return redirect()->back();

    }
    // İptal Etme İşlemi
    public function cancel($id) {
        $model = Order::findOrFail($id);
        $model->status = Order::STATUS_CANCELED;
        if($model->save()) {
            Session::flash('success_message', 'Sipariş başarılı bir şekilde iptal edildi!');
        }else {
            Session::flash('error_message', 'Beklenmedik bir hata meydana geldi!');
        }

        return redirect()->back();
    }
}
