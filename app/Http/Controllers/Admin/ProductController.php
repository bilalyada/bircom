<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Currency;
use App\Model\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{

    public function index()
    {
        $models = Product::orderBy('id', 'desc')->paginate(10);
        return view('admin.product.index', compact('models'));
    }

    public function create(Request $request)
    {
        if($request->post()) {
            $this->save($request);
        }
        $currencies = Currency::all();
        return view('admin.product.form', compact('currencies'));
    }

    public function update($id, Request $request)
    {
        $model = Product::findOrFail($id);
        $currencies = Currency::all();
        return view('admin.product.form', compact('model', 'currencies'));
    }

    // Kaydetme işlemi
    public function save(Request $request, $id = null) {

        $rules = [
            'name' => 'required|max:100',
            'currency_id' => 'required|integer|exists:currencies,id',
            'price' => 'required|numeric|regex:/^\d*(\.\d{1,2})?$/',
        ];

        $validator = Validator::make($request->all(), $rules);
        $errors = $validator->getMessageBag()->toArray();
        if(!$errors) {
            $model = new Product();
            if($id) {
                $model = Product::findOrFail($id);
            }
            $model->name = $request->post('name');
            $model->price = $request->post('price');
            $model->currency_id = $request->post('currency_id');
            $model->description = $request->post('description');

            $file = $request->file('file');
            if($file) {
                $model->image = $file->store(Product::STORE_PATH,['disk' => 'public']);
            }
            if($model->save()) {
                Session::flash('success_message', 'Ürün başarılı bir şekilde kaydedildi!');
            }else {
                Session::flash('error_message', 'Beklenmedik bir hata meydana geldi!');
            }

            return redirect()->route('admin.product');
        }else {
            Session::flash('error_message', 'Lütfen hataları düzeltip tekrar dene!');
            return redirect()->back()->withErrors($errors)->withInput();
        }
    }
    // Silme İşlemi
    public function delete($id) {
        $model = Product::findOrFail($id);
        if($model->delete()) {
            Session::flash('success_message', 'Ürün başarılı bir şekilde silindi!');
        }else {
            Session::flash('error_message', 'Beklenmedik bir hata meydana geldi!');
        }

        return redirect()->route('admin.product');

    }
}
