<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Model\Basket;
use App\Model\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class BasketController extends Controller
{
    public function index() {
        $models = Auth::user()->baskets;
        return view('member.basket.index', compact('models'));

    }
  public function add(Request $request)
  {
      $all = $request->all();

    if(@$all['product_id']) {
        $product_id = $all['product_id'];
        $count = @$all['product_count'] ? :1;
        $product = Product::findOrFail($product_id);

        $basket = Basket::where('user_id', Auth::id())->where('product_id' , $product_id)->first();
        $basket = $basket ? : new Basket();
        $basket->product_id = $product_id;
        $basket->user_id = Auth::id();
        $basket->count = $count;

        if($basket->save()) {
            Session::flash('success_message', 'Sepete ekleme başarılı!');
        }else {
            Session::flash('error_message', 'Beklenmedik bir hata meydana geldi!');
        }
    }else {
        Session::flash('error_message', 'Beklenmedik bir hata meydana geldi!');
    }

    return \redirect()->route('basket');
  }
}
