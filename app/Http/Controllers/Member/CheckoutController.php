<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Model\Order;
use App\Model\OrderItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class CheckoutController extends Controller
{
    public function index() {
        $models = Auth::user()->baskets;

        return view('member.checkout.index', compact('models'));

    }

    public function complete(Request $request) {
        $rules = [
            'payment_type' => 'required|max:20',
            'address' => 'required|max:255',
        ];

        $validator = Validator::make($request->all(), $rules);
        $errors = $validator->getMessageBag()->toArray();
        if(!$errors) {
            DB::beginTransaction();
            try {
                $model = new Order();
                $model->payment_type = $request->post('payment_type');
                $model->address = $request->post('address');

                if($model->save()) {

                    $baskets = Auth::user()->baskets;
                    foreach ($baskets as $basket) {
                        $item = new OrderItem();
                        $item->order_id = $model->id;
                        $item->product_id = $basket->product_id;
                        $item->count = $basket->count;
                        $item->price = $basket->product->price;
                        $item->total_price = $item->price*$item->count;
                        $item->currency_id = $basket->product->currency_id;
                        $item->save();
                        $basket->delete();
                    }
                    Session::flash('success_message', 'Sipariş başarılı bir şekilde alındı!');
                }
                else {
                    Session::flash('error_message', 'Beklenmedik bir hata meydana geldi!');
                }
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                Session::flash('error_message', 'Beklenmedik bir hata meydana geldi!');
            }

            return redirect('/');
        }else {
            Session::flash('error_message', 'Lütfen hataları düzeltip tekrar dene!');
            return redirect()->back()->withErrors($errors)->withInput();
        }
    }
}
