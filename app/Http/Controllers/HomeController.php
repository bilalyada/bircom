<?php

namespace App\Http\Controllers;


use App\Model\Product;

class HomeController extends Controller
{
    public function index()
    {
        $models = Product::orderBy('id', 'desc')->paginate(10);
        return view('home', compact('models'));
    }
}
