<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class SuperUser
{
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @param  string|null  $guard
   * @return mixed
   */
  public function handle($request, Closure $next, $guard = null)
  {
    if (!auth()->user() || @auth()->user()->type != User::ROLE_ADMIN) {
      return redirect(route('home'));
    }

    return $next($request);
  }
}
