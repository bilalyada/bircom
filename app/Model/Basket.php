<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Basket extends Model
{

    protected $table = 'baskets';
    protected $fillable = [
        'product_id', 'user_id', 'count'
    ];

    // Ürün
    public function product() {
        return $this->belongsTo(Product::class, 'product_id');
    }
    // Kullanıcı
    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }


    public function totalPriceWithCurrency() {
        return number_format($this->count*$this->product->price, 2, '.', ',').' '.$this->product->curency->readable;

    }
}
