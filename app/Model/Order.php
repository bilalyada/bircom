<?php

namespace App\Model;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Order extends Model
{

    const STATUS_NEW = 'NEW';
    const STATUS_CANCELED = 'CANCELED';
    const STATUS_APPROVED = 'APPROVED';

    const PAYMENT_TYPE_AT_THE_DOOR = 'AT_THE_DOOR';
    const PAYMENT_TYPE_CARD = 'CARD';

    protected $table = 'orders';
    protected $fillable = [
        'status','user_id', 'payment_type', 'address'
    ];

    // Kullanıcı
    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
    // Ürün
    public function items() {
        return $this->hasMany(OrderItem::class, 'order_id');
    }
    public function createdAt() {
        return Carbon::parse($this->created_at)->format('d.m.Y h:i');
    }
    // Durum
    public function status() {
        return @Order::statues()[$this->status] ? : $this->status;
    }
    // Duruma göre renk classı
    public function color() {
        return @Order::colors()[$this->status] ? : $this->status;
    }
    // Ödeme Tipi
    public function paymentType() {
        return @Order::pamentTypes()[$this->payment_type] ? : $this->payment_type;
    }

    // Renkler
    public static function colors() {
        return [
            self::STATUS_NEW => 'badge badge-warning-border',
            self::STATUS_APPROVED => 'badge badge-success-border',
            self::STATUS_CANCELED => 'badge badge-danger-border',
        ];
    }
    // Durumlar
    public static function statues() {
        return [
            self::STATUS_NEW => 'Bekliyor',
            self::STATUS_APPROVED => 'Onaylandı',
            self::STATUS_CANCELED => 'İptal Edildi',
        ];
    }

    // Durumlar
    public static function pamentTypes() {
        return [
            self::PAYMENT_TYPE_AT_THE_DOOR => 'Kapıda Ödeme',
            self::PAYMENT_TYPE_CARD => 'Banka/Kredi Kartı İle Ödeme',
        ];
    }
    protected static function boot(){
        parent::boot();

        static::creating(function ($model) {
            $model->status = self::STATUS_NEW;
            $model->user_id = Auth::id();
        });
    }

}
