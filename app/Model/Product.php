<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    const STORE_PATH = 'products';

    protected $table = 'products';
    protected $fillable = [ 'name', 'price', 'currency_id', 'description', 'image'];

    public function priceWithCurrency() {
        return number_format($this->price, 2, '.', ',').' '.$this->curency->readable;
    }
    public function curency() {
        return $this->belongsTo(Currency::class, 'currency_id');
    }
    public function createdAt() {
        return Carbon::parse($this->created_at)->format('d.m.Y h:i');
    }
    public function image() {
        return $this->image ? url('storage/'.$this->image) : asset('site/img/product1.png');
    }
}
