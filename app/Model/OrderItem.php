<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $table = 'order_items';
    protected $fillable = [
        'product_id', 'price', 'currency_id', 'total_price'
    ];

    // Ürün
    public function product() {
        return $this->belongsTo(Product::class, 'product_id')->withTrashed();
    }

    // Total Fiyatı para birimi ile
    public function totalPriceWithCurrency() {
        return number_format($this->total_price, 2, '.', ',').' '.$this->curency->readable;
    }
    // Fiyat Para Birimi ile
    public function priceWithCurrency() {
        return number_format($this->price, 2, '.', ',').' '.$this->curency->readable;
    }
    // Para birimi
    public function curency() {
        return $this->belongsTo(Currency::class, 'currency_id');
    }
}
