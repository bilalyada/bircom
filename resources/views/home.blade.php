@extends('layouts.app')

@section('content')

    <section class="hero-area2 hero-area3 bgimage">
        <div class="bg_image_holder">
            <img src="{{asset('site/img/hero-image01.png')}}" alt="background-image">
        </div>
        <div class="hero-content content_above">
            <div class="content-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="hero__content__title">
                                <h1>Bircom</h1>
                            </div><!-- end .hero__btn-area-->
                            <div class="search-area">
                                <div class="row">
                                    <div class="col-lg-6 offset-lg-3">

                                        <div class="search_box2">
                                            <form action="#">
                                                <input type="text" class="text_field" placeholder="Search your products...">
                                                <button type="submit" class="search-btn btn--lg btn-primary">Search Now</button>
                                            </form>
                                        </div><!-- end .search_box -->

                                    </div>
                                </div>
                            </div><!--start .search-area -->
                        </div><!-- end .col-md-12 -->
                    </div>
                </div>
            </div><!-- end .contact_wrapper -->
        </div><!-- end hero-content -->
    </section><!-- ends: .hero-area -->
    @include('layouts.message')

    <section class="product-grid p-bottom-100 p-top-50">
        <div class="container">
            <div class="row">
                <!-- Start .product-list -->
                <div class="col-md-12 product-list">
                    <div class="row">
                        @foreach($models as $model)
                        <div class="col-lg-4 col-md-6">

                            <div class="product-single latest-single">

                                <div class="product-thumb">
                                    <figure>
                                        <img src="{{$model->image()}}" alt="" class="img-fluid" style="max-height: 230px;">
                                        <figcaption>
                                            <ul class="list-unstyled">
                                                <li><a  @if(\Illuminate\Support\Facades\Auth::check()) class="add-cart" data-title="{{$model->name}}" data-id="{{$model->id}}" @else href="{{route('login')}}" @endif><span class="icon-basket"></span></a></li></ul>
                                        </figcaption>
                                    </figure>
                                </div>
                                <!-- Ends: .product-thumb -->
                                <div class="product-excerpt">
                                    <h5>
                                        <a href="#">{{$model->name}}</a>
                                    </h5>

                                    <ul class="product-facts clearfix">
                                        <li class="price">{{$model->priceWithCurrency()}}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>


                    <!-- Start Pagination -->
                    <nav class="pagination-default">
                        {{ $models->appends(request()->input())->links() }}
                    </nav><!-- Ends: .pagination-default -->


                </div>
                <!-- Ends: .product-list -->
            </div>
        </div>
    </section><!-- ends: .product-grid -->


    <div class="modal fade rating_modal" id="myModal1" tabindex="-1" role="dialog" >
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="title"></h4>
                </div>

                <div class="modal-body">
                    <form action="{{route('basket.add')}}" method="post">
                        @csrf
                        <input name="product_id" type="hidden" id="product_id"/>
                        <ul>
                            <li>
                                <p>Adet</p>
                                <div class="right_content">
                                    <div class="select-wrap">
                                        <select name="product_count">
                                            @for($i=1; $i<10; $i++)
                                            <option value="{{$i}}">{{$i}}</option>
                                            @endfor
                                        </select>
                                        <span class="icon-arrow-down"></span>
                                    </div>
                                </div>
                            </li>
                        </ul>

                        <button type="submit" class="btn btn-md btn-primary">Sepete Ekle</button>
                        <button class="btn modal_close" data-dismiss="modal">Kapat</button>
                    </form>
                    <!-- end /.form -->
                </div>
                <!-- end /.modal-body -->
            </div>
        </div>
    </div>

@endsection
