@extends('admin.layouts.app')
@section('content')
    <div class="row">
        <div class="col-sm-8 col-4">
            <h4 class="page-title">Ürünler</h4>
        </div>
        <div class="col-sm-4 col-8 text-right m-b-30">
            <a class="btn btn-success pull-right" href="{{route('admin.product.create')}}"><i class="fa fa-plus"></i> Ekle</a>
        </div>
    </div>


  <div class="row">
    <div class="col-md-12">
      <div class="table-responsive">
        <table class="table table-border custom-table m-b-0">
          <thead>
          <tr>
            <th>ID</th>
              <th>Görsel</th>
            <th>Ad</th>
            <th>Fiyat</th>
            <th>Tarih</th>
            <th class="text-right">İşlemler</th>
          </tr>
          </thead>
          <tbody>
          @if($models->count())
              @foreach($models as $model)
                <tr>
                  <td>#{{$model->id}}</td>
                  <td><img src="{{$model->image()}}" class="w-40"/></td>
                  <td>{{$model->name}}</td>
                  <td>{{$model->priceWithCurrency()}}</td>
                  <td>{{$model->createdAt()}}</td>
                  <td class="text-right">
                    <div class="dropdown dropdown-action">
                      <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                      <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="{{route('admin.product.update', ['id' => $model->id])}}"><i class="fa fa-pencil m-r-5"></i> Güncelle</a>
                        <a class="dropdown-item action" href="javascript:;" data-action="{{route('admin.product.delete', ['id' => $model->id])}}"><i class="fa fa-trash-o m-r-5"></i> Sil</a>
                      </div>
                    </div>
                  </td>
                </tr>
              @endforeach
          @else
              <tr class="text-center">
                  <td colspan="6">Herhangi bir sonuç bulunamadı.</td>
              </tr>
          @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="row staff-grid-row mt-3">
    <div class="col-sm-12">
      <div class="see-all text-center">
        {{ $models->appends(request()->input())->links() }}
      </div>
    </div>
  </div>
@endsection
