@extends('admin.layouts.app')
@section('content')
    <div class="row">
        <div class="col-sm-8 col-4">
            <h4 class="page-title">{{ __(@$model  ? 'Ürün Güncelle':'Ürün Ekle') }}</h4>
        </div>
        <div class="col-sm-4 col-8 text-right m-b-30">
            <a class="btn btn-success pull-right" href="{{route('admin.product')}}"><i class="fa fa-list"></i> {{__('Listeye Dön')}}</a>
        </div>
    </div>

  <div class="row">
      <div class="col-lg-12">
          <div class="card-box">
              <form method="post" enctype="multipart/form-data">
                  @csrf
                  <div class="form-group row">
                      <label class="col-form-label col-md-2">{{__('Ürün Adı')}}</label>
                      <div class="col-md-10">
                          <input type="text" class="form-control" name="name" value="{{old('name', @$model->name)}}">
                          @error('name')
                          <small class="form-text text-muted">{{$message}}</small>
                          @enderror
                      </div>
                  </div>
                  <div class="form-group row">
                      <label class="col-form-label col-md-2">{{__('Görsel')}}</label>
                      <div class="col-md-10">
                          <input class="form-control" type="file" name="file" accept="image/x-png,image/gif,image/jpeg" value="{{old('file')}}">
                      </div>
                      @error('file')
                      <small class="form-text text-muted">{{$message}}</small>
                      @enderror
                  </div>
                  <div class="form-group row">
                      <label class="col-form-label col-md-2">{{__('Fiyat')}}</label>
                      <div class="col-md-10">
                          <input type="text" class="form-control" name="price" value="{{old('price', @$model->price)}}">
                          @error('price')
                          <small class="form-text text-muted">{{$message}}</small>
                          @enderror
                      </div>
                  </div>
                  <div class="form-group row">
                      <label class="col-form-label col-md-2">{{__('Para Birimi')}}</label>
                      <div class="col-md-10">
                          <select class="form-control" name="currency_id">
                              <option value="">-- Seç --</option>
                              @foreach($currencies as $currency)
                              <option value="{{$currency->id}}" {{@$model->currency_id==$currency->id || old('currency_id')==$currency->id ? 'selected': ''}}>{{$currency->readable}}</option>
                              @endforeach
                          </select>
                          @error('currency_id')
                          <small class="form-text text-muted">{{$message}}</small>
                          @enderror
                      </div>
                  </div>
                  <div class="form-group row">
                      <label class="col-form-label col-md-2">{{__('Açıklama')}}</label>
                      <div class="col-md-10">
                          <textarea rows="5" cols="5" class="form-control" name="description" placeholder="Ürün açıklaması yazın...">{{old('description', @$model->description)}}</textarea>
                          @error('description')
                          <small class="form-text text-muted">{{$message}}</small>
                          @enderror
                      </div>
                  </div>
                  <div class="m-t-20 text-center">
                      <button class="btn btn-success btn-lg" type="submit">{{__('Kaydet')}}</button>
                  </div>
              </form>
          </div>
      </div>
  </div>
@endsection
