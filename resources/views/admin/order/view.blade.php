@extends('admin.layouts.app')
@section('content')
    <div class="row">
        <div class="col-sm-8 col-4">
            <h4 class="page-title">#{{$model->id}}</h4>
        </div>
        <div class="col-sm-4 col-8 text-right m-b-30">
            <a class="btn btn-success pull-right action" href="javascript:;" data-action="{{route('admin.order.approve', ['id' =>$model->id])}}"><i class="fa fa-check"></i> Onayla</a>
            <a class="btn btn-danger pull-right action" href="javascript:;" data-action="{{route('admin.order.cancel', ['id' =>$model->id])}}"><i class="fa fa-close"></i> İptal Et</a>
        </div></div>

    <div class="card-box">
        <div class="row">
            <div class="col-md-12">
                <div class="profile-view">
                    <div class="profile-basic">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="profile-info-left">
                                    <ul class="personal-info">
                                        <li>
                                            <span class="title">Kullanıcı:</span>
                                            <span class="text">{{$model->user->name}}</span>
                                        </li>
                                        <li>
                                            <span class="title">Kullanıcı Adı:</span>
                                            <span class="text">{{$model->user->username}}</span>
                                        </li>
                                        <li>
                                            <span class="title">Email:</span>
                                            <span class="text">{{$model->user->email}}</span>
                                        </li>
                                        <li>
                                            <span class="title">Adres:</span>
                                            <span class="text">{{$model->address}}</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <ul class="personal-info">
                                    <li>
                                        <span class="title">Ödeme Tipi:</span>
                                        <span class="text">{{$model->paymentType()}}</span>
                                    </li>
                                    <li>
                                        <span class="title">Durum:</span>
                                        <span class="text {{$model->color()}}">{{$model->status()}}</span>
                                    </li>
                                    <li>
                                        <span class="title">Sipariş Tarihi:</span>
                                        <span class="text">{{$model->createdAt()}}</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card-box">
        <h3>Ürünler</h3>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-border custom-table m-b-0">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Görsel</th>
                            <th>Ürün</th>
                            <th>Adet</th>
                            <th>Fiyat</th>
                            <th>Toplam</th>

                        </tr>
                        </thead>
                        <tbody>
                        @if($model->items)
                            @foreach($model->items as $item)
                                <tr>
                                    <td>#{{$item->id}}</td>
                                    <td>
                                        <img src="{{$item->product->image()}}" style="max-height: 40px"/>
                                    </td>
                                    <td>
                                        {{$item->product->name}}
                                    </td>
                                    <td>
                                        {{$item->count}}
                                    </td>
                                    <td>
                                        {{$item->priceWithCurrency()}}
                                    </td>
                                    <td>
                                        {{$item->totalPriceWithCurrency()}}
                                    </td>

                                </tr>
                            @endforeach
                        @else
                            <tr class="text-center">
                                <td colspan="6">Herhangi bir sonuç bulunamadı!</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
