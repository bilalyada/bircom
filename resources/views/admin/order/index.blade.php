@extends('admin.layouts.app')
@section('content')
  <div class="row">
    <div class="col-sm-4 col-4">
      <h4 class="page-title">Siparişler</h4>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="table-responsive">
        <table class="table table-border custom-table m-b-0">
          <thead>
          <tr>
            <th>ID</th>
            <th>Kullanıcı</th>
            <th>Durum</th>
            <th>Tarih</th>
            <th class="text-right">İşlemler</th>
          </tr>
          </thead>
          <tbody>
          @if($models->count())
            @foreach($models as $model)
              <tr>
                <td>#{{$model->id}}</td>
                <td>
                  {{$model->user->name}}
                </td>
                <td>
                    <span class="{{$model->color()}}">{{$model->status()}}</span>
                </td>
                <td>{{$model->createdAt()}}</td>
                <td class="text-right">
                  <div class="dropdown dropdown-action">
                    <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                    <div class="dropdown-menu dropdown-menu-right">
                      <a class="dropdown-item" href="{{route('admin.order.view' ,['id' =>$model->id])}}"><i class="fa fa-eye m-r-5"></i> Detay</a>
                    </div>
                  </div>
                </td>
              </tr>
            @endforeach
          @else
              <tr class="text-center">
                  <td colspan="8">Herhangi bir sonuç bulunamadı!</td>
              </tr>
          @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="row staff-grid-row mt-3">
    <div class="col-sm-12">
      <div class="see-all text-center">
        {{ $models->appends(request()->input())->links() }}
      </div>
    </div>
  </div>
@endsection
