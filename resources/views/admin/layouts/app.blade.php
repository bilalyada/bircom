<!DOCTYPE html>
<html lang="tr">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
  <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
  <title>Bircom - Admin Paneli</title>
  <link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,500,600,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/bootstrap.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/font-awesome.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/fullcalendar.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/select2.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/bootstrap-datetimepicker.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('admin/plugins/morris/morris.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/style.css') }}">
  <!--[if lt IE 9]>
  <script src="{{ asset('admin/js/html5shiv.min.js') }}"></script>
  <script src="{{ asset('admin/js/respond.min.js') }}"></script>
  <![endif]-->
</head>

<body>
<div class="main-wrapper">
  <div class="header">
    <div class="header-left">
      <a href="{{route('admin.dashboard')}}" class="logo">
          <img src="{{asset('admin/img/logo.png')}}" width="40" height="40" alt="">
      </a>
    </div>
    <div class="page-title-box pull-left">
    </div>
    <a id="mobile_btn" class="mobile_btn pull-left" href="#sidebar"><i class="fa fa-bars" aria-hidden="true"></i></a>
    <ul class="nav user-menu pull-right">
      <li class="nav-item dropdown has-arrow">
        <a href="#" class="dropdown-toggle nav-link user-link" data-toggle="dropdown">
                        <span class="user-img">
							<img class="rounded-circle" src="{{asset('admin/img/user.jpg')}}" width="40" alt="Admin">
							<span class="status online"></span>
						</span>
          <span>{{\Illuminate\Support\Facades\Auth::user()->name}}</span>
        </a>
        <div class="dropdown-menu">

          <a class="dropdown-item" href="{{ route('logout') }}"
              onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
          >Çıkış Yap</a>
        </div>
      </li>
    </ul>
    <div class="dropdown mobile-user-menu pull-right">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
      <div class="dropdown-menu dropdown-menu-right">
        <a class="dropdown-item" href="{{ route('logout') }}"
            onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
        >Çıkış Yap</a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          @csrf
        </form>
      </div>
    </div>
  </div>
  <div class="sidebar" id="sidebar">
    <div class="sidebar-inner slimscroll">
      <div id="sidebar-menu" class="sidebar-menu">
        <ul>
          <li class="{{request()->url() == route('admin.product') ? 'active': ''}}">
            <a href="{{route('admin.product')}}"><i class="fa fa-book"></i> Ürünler</a>
          </li>
          <li class="{{request()->url() == route('admin.order') ? 'active': ''}}">
            <a href="{{route('admin.order')}}"><i class="fa fa-shopping-cart"></i> Siparişler</a>
          </li>

        </ul>
      </div>
    </div>
  </div>
  <div class="page-wrapper">
    <div class="content container-fluid">
      @yield('content')
    </div>
  </div>
</div>
<div id="confirm_modal" class="modal custom-modal fade fade">
    <button type="button" class="close" data-dismiss="modal">×</button>
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content"><button data-dismiss="modal" class="close" type="button">×</button>
            <div class="modal-header">
                <h3 class="modal-title">Dikkat</h3>
            </div>
            <div class="modal-body">
                <p> Bu işlemi yapmak istediğinize emin misiniz?</p>
                <form method="post" id="confirm_form">
                    @csrf
                    <div class="m-t-50">
                        <button class="btn btn-danger btn-lg" data-dismiss="modal">Vazgeç</button>
                        <button class="btn btn-success btn-lg" type="submit">Onayla</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="notification-popup hide">
    <p>
        <span class="notification-text"></span>
    </p>
</div>
<div class="sidebar-overlay" data-reff=""></div>
<script type="text/javascript" src="{{ asset('admin/js/jquery-3.2.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/popper.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/jquery.slimscroll.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/bootstrap-datetimepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/plugins/morris/morris.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/plugins/raphael/raphael-min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/app.js') }}"></script>

<script>
  var csrf_token= '{{csrf_token()}}';
  $('.action').on('click', function () {
      var action = $(this).data('action');
      $('#confirm_modal').modal('show');
      $('#confirm_form').attr('action', action);
  });

function alert(notificationText, newClass) {
    var notificationPopup = $('.notification-popup ');
    notificationPopup.find('.notification-text').text(notificationText);
    notificationPopup.removeClass('hide success');

    if (newClass)
        notificationPopup.addClass(newClass);

    notificationTimeout = setTimeout(function() {
        notificationPopup.addClass('hide');
    }, 3000);
}
    @if(session()->has('success_message'))
        alert(' {{ session()->get('success_message') }}', 'success')
    @endif
    @if(session()->has('error_message'))
        alert(' {{ session()->get('error_message') }}', 'error')
    @endif
</script>
</body>

</html>
