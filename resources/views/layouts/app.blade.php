<!doctype HTML>
<html {{ str_replace('_', '-', app()->getLocale()) }}>
<head>
    <meta charset="UTF-8">

    <!-- viewport meta -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Bircom">
    <meta name="keywords" content="Bircom">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Bircom') }}</title>

    <link href="https://fonts.googleapis.com/css?family=Work+Sans:400,500,600" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('site/css/plugin.min.css') }}">

    <link rel="stylesheet" href="{{ asset('site/style.css') }}">

    <!-- endinject -->

    <!-- Favicon Icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('site/img/favicon-32x32.png') }}">
</head>
<body class="preload">

<div class="menu-area">
    <div class="top-menu-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="menu-fullwidth">
                        <div class="logo-wrapper">
                            <div class="logo logo-top">
                                <a href="{{url('/')}}"><img src="{{asset('site/img/logo.png')}}" alt="logo image" class="img-fluid"></a>
                            </div>
                        </div>

                        <div class="menu-container">
                            <div class="d_menu">

                                <nav class="navbar navbar-expand-lg mainmenu__menu">
                                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                                            data-target="#bs-example-navbar-collapse-1"
                                            aria-controls="bs-example-navbar-collapse-1"
                                            aria-expanded="false" aria-label="Toggle navigation">
                                        <span class="navbar-toggler-icon icon-menu"></span>
                                    </button>
                                    <!-- Collect the nav links, forms, and other content for toggling -->
                                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                        <ul class="navbar-nav">
                                            <li>
                                                <a href="{{ url('/') }}">Anasayfa</a>
                                            </li>
                                            @if(!\Illuminate\Support\Facades\Auth::check())
                                            <li>
                                                <a href="{{ url('giris-yap') }}">Giriş Yap</a>
                                            </li>
                                            <li>
                                                <a href="{{ url('kayit-ol') }}">Kaydol</a>
                                            </li>
                                            @endif
                                        </ul>
                                    </div>
                                    <!-- /.navbar-collapse -->
                                </nav>

                            </div>
                        </div>


                        <div class="author-menu">
                            <div class="author-area">
                                <div class="search-wrapper">
                                    <div class="nav_right_module search_module">
                                        <span class="icon-magnifier search_trigger"></span>

                                        <div class="search_area">
                                            <form action="#">
                                                <div class="input-group input-group-light">
                                <span class="icon-left" id="basic-addon1">
                                    <i class="icon-magnifier"></i>
                                </span>
                                                    <input type="text" class="form-control search_field"
                                                           placeholder="Ürüna adı yazarak ürün ara...">
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                @if(\Illuminate\Support\Facades\Auth::check())

                                <div class="author-author__info has_dropdown">
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                    <div class="author__avatar online">
                                        <img src="{{asset('site/img/user-avater.png')}}" alt="user avatar"
                                             class="rounded-circle">
                                    </div>

                                    <div class="dropdown dropdown--author">
                                        <div class="author-credits d-flex">
                                            <div class="author__avatar">
                                                <img src="{{asset('site/img/user-avater.png')}}" alt="user avatar"
                                                     class="rounded-circle">
                                            </div>

                                        </div>
                                        <ul>

                                            <li>
                                                <a href="{{ route('logout') }}"
                                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                    <span class="icon-logout"></span>Çıkış Yap</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                @endif
                            </div>
                            @if(\Illuminate\Support\Facades\Auth::check())
                            <div class="mobile_content ">
                                <span class="icon-user menu_icon"></span>

                                <!-- offcanvas menu -->
                                <div class="offcanvas-menu closed">
                                    <span class="icon-close close_menu"></span>
                                    <div class="author-author__info">
                                        <div class="author__avatar v_middle">
                                            <img src="{{asset('site/img/user-avater.png')}}" alt="user avatar">
                                        </div>
                                    </div>
                                    <!--end /.author-author__info-->

                                    <!--start .author__notification_area -->

                                    <div class="dropdown dropdown--author">
                                        <ul>
                                            <li>
                                                <a href="{{ route('logout') }}"
                                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                    <span class="icon-logout"></span>Çıkış Yap</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>


                    </div>
                </div>
            </div>
            <!-- end /.row -->
        </div>
        <!-- end /.container -->
    </div>
    <!-- end  -->
</div>
@yield('content')
<footer class="footer-area footer--light">
    <div class="footer-big">
        <!-- start .container -->
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-sm-6">
                    <div class="footer-widget">

                        <div class="widget-about">
                            <img src="{{asset('site/img/footer-logo.png')}}" alt="" class="img-fluid">
                            <p>Pellentesque facilisis the ullamcorp keer sapien interdum is the magna pellentesque
                                kequis
                                hasellus keur condimentum eleifend.</p>
                            <ul class="contact-details">
                                <li>
                                    <span class="icon-earphones"></span>
                                    Call Us:
                                    <a href="tel:344-755-111">344-755-111</a>
                                </li>
                                <li>
                                    <span class="icon-envelope-open"></span>
                                    <a href="mailto:support@bircom.com">support@bircom.com</a>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <!-- Ends: .footer-widget -->
                </div>
                <!-- end /.col-md-4 -->

                <div class="col-lg-3 col-sm-6">
                    <div class="footer-widget">
                        <div class="footer-menu footer-menu--1">
                            <h5 class="footer-widget-title">Popular Category</h5>
                            <ul>
                                <li>
                                    <a href="#">Wordpress</a>
                                </li>
                                <li>
                                    <a href="#">Plugins</a>
                                </li>
                                <li>
                                    <a href="#">Joomla Template</a>
                                </li>
                                <li>
                                    <a href="#">Admin Template</a>
                                </li>
                                <li>
                                    <a href="#">HTML Template</a>
                                </li>
                            </ul>
                        </div>
                        <!-- end /.footer-menu -->
                    </div>
                    <!-- Ends: .footer-widget -->
                </div>
                <!-- end /.col-md-3 -->

                <div class="col-lg-3 col-sm-6">
                    <div class="footer-widget">
                        <div class="footer-menu">
                            <h5 class="footer-widget-title">Our Company</h5>
                            <ul>
                                <li>
                                    <a href="#">About Us</a>
                                </li>
                                <li>
                                    <a href="#">How It Works</a>
                                </li>
                                <li>
                                    <a href="#">Affiliates</a>
                                </li>
                                <li>
                                    <a href="#">Testimonials</a>
                                </li>
                                <li>
                                    <a href="#">Contact Us</a>
                                </li>
                                <li>
                                    <a href="#">Plan & Pricing</a>
                                </li>
                                <li>
                                    <a href="#">Blog</a>
                                </li>
                            </ul>
                        </div>
                        <!-- end /.footer-menu -->
                    </div>
                    <!-- Ends: .footer-widget -->
                </div>
                <!-- end /.col-lg-3 -->

                <div class="col-lg-3 col-sm-6">
                    <div class="footer-widget">
                        <div class="footer-menu no-padding">
                            <h5 class="footer-widget-title">Help Support</h5>
                            <ul>
                                <li>
                                    <a href="#">Support Forum</a>
                                </li>
                                <li>
                                    <a href="#">Terms & Conditions</a>
                                </li>
                                <li>
                                    <a href="#">Support Policy</a>
                                </li>
                                <li>
                                    <a href="#">Refund Policy</a>
                                </li>
                                <li>
                                    <a href="#">FAQs</a>
                                </li>
                                <li>
                                    <a href="#">Buyers Faq</a>
                                </li>
                                <li>
                                    <a href="#">Sellers Faq</a>
                                </li>
                            </ul>
                        </div>
                        <!-- end /.footer-menu -->
                    </div>
                    <!-- Ends: .footer-widget -->
                </div>
                <!-- Ends: .col-lg-3 -->
            </div>
            <!-- end /.row -->
        </div>
        <!-- end /.container -->
    </div>
    <!-- end /.footer-big -->

    <div class="mini-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="copyright-text">
                        <p>&copy; 2021
                            <a href="#">Bircom</a>.
                        </p>
                    </div>

                    <div class="go_top">
                        <span class="icon-arrow-up"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>


<script src="{{ asset('site/js/plugins.min.js') }}"></script>

<script src="{{ asset('site/js/script.min.js') }}"></script>
<script>
    var csrf_token= '{{csrf_token()}}';
    $(document).on("click",".add-cart",function() {
        var element = $(this);
        var id= element.attr('data-id');
        var title= element.attr('data-title');
        $('.title').text(title);
        $('#product_id').val(id);
        $('#myModal1').modal('show');
    });
</script>
</body>

</html>
