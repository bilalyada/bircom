<section class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb-contents">
                    <h2 class="page-title">{{$page_title}}</h2>
                    <div class="breadcrumb">
                        <ul>
                            <li>
                                <a href="{{url('/')}}">Anasayfa</a>
                            </li>
                            <li class="active">
                                <a>{{$page_title}}</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

