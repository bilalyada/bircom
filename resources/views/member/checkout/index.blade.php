@extends('layouts.app')

@section('content')

    @include('layouts.breadcrumb', ['page_title' => 'Alışverişi Tamamla'])

    <section class="dashboard-area p-top-100 p-bottom-70">
        <div class="dashboard_contents">
            <div class="container">
                <form action="{{route('checkout.complete')}}" class="setting_form" method ="post">
                    @csrf
                    <div class="row">
                        <div class="col-lg-6 col-md-12">

                            <div class="information_module payment_options">
                                <div class="toggle_title">
                                    <h4>Ödeme Tipi</h4>
                                </div>

                                <ul>
                                    <li>
                                        <div class="custom-radio">
                                            <input type="radio" id="opt1" name="payment_type" class="" value="{{\App\Model\Order::PAYMENT_TYPE_AT_THE_DOOR}}">
                                            <label for="opt1">
                                                <span class="circle"></span>Kapıda Ödeme</label>
                                            @error('payment_type')
                                            <small class="form-text text-danger">{{$message}}</small>
                                            @enderror
                                        </div>
                                    </li>

                                </ul>

                                <div class="payment_info modules__content">
                                    <div class="form-group">
                                        <label for="card_number">Adres</label>
                                        <input type="text" class="text_field" placeholder="Adres" name="address">
                                        @error('address')
                                        <small class="form-text text-danger">{{$message}}</small>
                                        @enderror
                                    </div>

                                </div>

                            </div>

                            <button type="submit" class="btn btn--md btn-primary">Tamamla</button>
                        </div>

                        <div class="col-lg-6 col-md-12">

                            <div class="information_module order_summary">
                                <div class="toggle_title">
                                    <h4>Özet</h4>
                                </div>

                                <ul>
                                    @foreach($models as $model)
                                    <li class="item">
                                        <a>{{$model->product->name}}</a>
                                        <span>{{$model->totalPriceWithCurrency()}}</span>
                                    </li>
                                    @endforeach

                                </ul>
                            </div>

                        </div>
                    </div><!-- ends: .row -->
                </form><!-- ends: form -->
            </div>
        </div><!-- ends: .dashboard_contents -->
    </section><!-- ends: .dashboard-area -->


@endsection


