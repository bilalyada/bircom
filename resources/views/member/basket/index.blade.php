@extends('layouts.app')

@section('content')
    @include('layouts.breadcrumb', ['page_title' => 'Sepet'])

    <section class="cart_area section--padding bgcolor">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="product_archive added_to__cart">
                    @if(!$models->isEmpty())
                    <div class="table-responsive single_product">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col"><h4>Ürün</h4></th>
                                <th scope="col"><h4>Tutar</h4></th>
                                <th scope="col"><h4>Adet</h4></th>
                                <th scope="col"><h4>Toplam</h4></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($models as $model)
                            <tr>
                                <td colspan="1">
                                    <div class="product__description">
                                        <div class="p_image"><img style="max-height: 80px;" src="{{$model->product->image()}}" alt="Purchase image"></div>
                                        <div class="short_desc">
                                            <a>
                                                <h6>{{$model->name}}</h6>
                                            </a>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="item_price">
                                        <span>{{$model->product->priceWithCurrency()}}</span>
                                    </div>
                                </td>
                                <td>
                                    {{$model->count}}
                                </td>
                                <td>
                                    <div class="item_price">
                                        <span>{{$model->totalPriceWithCurrency()}}</span>
                                    </div>
                                </td>

                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div><!-- ends: .single_product -->

                    <div class="row">
                        <div class="col-md-6 offset-md-6">
                            <div class="cart_calculation text-right">

                                <a href="{{route('checkout')}}" class="btn btn--md checkout_link btn-primary">Tamamla</a>
                            </div>
                        </div><!-- end .col-md-12 -->
                    </div><!-- end .row -->
                    @else
                        <div class="row text-center p-top-50">
                            <div class="col-md-12">
                                <p>Herhangi bir sonuç bulunamadı!</p>
                            </div>
                        </div>
                    @endif
                </div><!-- end .added_to__cart -->

            </div><!-- end .col-md-12 -->
        </div><!-- end .row -->
    </div><!-- end .container -->
</section><!-- ends: .cart_area -->

@endsection


