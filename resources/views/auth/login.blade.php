@extends('layouts.app')

@section('content')
    @include('layouts.breadcrumb', ['page_title' => 'Giriş Yap'])

    <section class="login_area section--padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="cardify login">
                            <div class="login--header">
                                <h3>Hoş geldin</h3>
                                <p>Kullanıcı adı ve şifreni yazarak giriş yap.</p>
                            </div><!-- end .login_header -->

                            <div class="login--form">
                                <div class="form-group">
                                    <label for="user_name">Email</label>
                                    <input id="user_name" name="email" type="text" class="text_field {{$errors->has('email') ? ' has-error' : '' }}" placeholder="Email" value="{{ old('email') ?: '' }}">
                                    @error('email')
                                    <p class="text-error">{{$message}}</p>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="pass">Şifre</label>
                                    <input id="pass" type="password" class="text_field" placeholder="Şifre" name="password" value="{{ old('password') ?: '' }}">
                                    @error('password')
                                    <p class="text-error">{{$message}}</p>
                                    @enderror
                                </div>

                                <button class="btn btn--md btn-primary" type="submit">Giriş Yap</button>

                                <div class="login_assist">
                                    <p class="signup">Henüz bir hesabın yok mu?
                                        <a href="{{ route('register') }}">Kaydol</a>?</p>
                                </div>
                            </div><!-- end .login--form -->
                        </div><!-- end .cardify -->
                    </form>
                </div><!-- end .col-md-6 -->
            </div><!-- end .row -->
        </div><!-- end .container -->
    </section><!-- ends: .login_area -->




@endsection
